const express = require('express');

const router = express.Router();


const {
	create,
	getAllProducts,
	getAll,
	oneProduct,
	co,
	allco,
	getAllOrders,
	updateProduct,
	archiveProduct,
	unArchive,
	homeProducts,
	deleteCheckout,
	deleteProduct
} = require('./../controllers/productControllers');

const {verifyAdmin, decode, verify} = require('./../auth');


//Create a product
	//Only admin can create
router.post('/create', verifyAdmin, async (req, res) => {
	try{
		create(req.body).then(result => res.send(result))
	}catch(err){
		res.status(500).json(err)
	}
})


//Get all ACTIVE products
router.get('/all-products', verify, async (req, res) => {
	try{
		await getAllProducts().then(result => res.send(result))
	}catch(err){
		res.status(500).json(err)
	}

})


//Both ACTIVE and INACTIVE Products
router.get('/adminProducts', verifyAdmin, async (req, res) => {
	try{
		await getAll().then(result => res.send(result))
	}catch(err){
		res.status(500).json(err)
	}
})




//Show all ACTIVE products in home page
router.get('/home-products', async (req, res) => {
	try{
		await homeProducts().then(result => res.send(result))
	}catch(err){
		res.status(500).json(err)
	}

})


//Get one product
router.get('/:_id', verify, async (req, res) => {
	const id = req.params._id;

	try{
		const product1 = await oneProduct(id).then(result => res.send(result))
	}catch(err){
		res.status(500).json(err)
	}
})



//User check-out
router.put('/check-out', verify, async (req, res) => {
	const user = decode(req.headers.authorization).isAdmin
	const buy = {
		userId: decode(req.headers.authorization).id,
		productId: req.body.productId,
		productName: req.body.productName,
		totalAmount: req.body.totalAmount
	}
	if(user == false){
	try{
		await co(buy).then(result => res.send(result))
	}catch(err){
		res.status(500).json(err)
	}
}else {
	res.send(`Only registered users can purchase items.`)
}
})



router.post('/all-checkout', verify, async (req, res) => {
	const uid = req.body.userId
	try{
		await allco(uid).then(result => res.send(result))
	}catch(err){
		res.status(500).json(err)
	}
})


//CANCEL A CHECKOUT BY USER
	//Only user can cancel
router.delete('/:_id/deleteco', verify, async (req, res) => {
	const oid = req.params._id

	try{
		await deleteCheckout(oid).then(result => res.send(result))
	}catch(err){
		res.status(500).json(err)
	}
})



//Get all orders
	//only admin
router.post('/all-orders', verifyAdmin, async (req, res) => {
	try{
		await getAllOrders().then(result => res.send(result))
	}catch(err){
		res.status(500).json(err)
	}
})



//Update product description
	//Only admin can update
router.put('/:productId/update', verifyAdmin, async (req, res) => {
	try{
		await updateProduct(req.params.productId, req.body).then(result => res.send(result))
	}catch(err){
		res.status(500).json(err)
	}
})



//Archive a product
	//Only admin can update 
router.patch('/:productId/archive', verifyAdmin, async (req, res) => {
	try{
		await archiveProduct(req.params.productId).then(result => res.send(result))
	}catch(err){
		res.status(500).json(err)
	}
})



//Unarchive a product
	//Only admin can unarchive
router.patch('/:productId/unarchive', verifyAdmin, async (req, res) => {
	try{
		await unArchive(req.params.productId).then(result => res.send(result))
	}catch(err){
		res.status(500).json(err)
	}
})



//Delete a product
	//Only admin can delete a course
router.delete('/:productId/delete', verifyAdmin, async (req, res) => {
	try{
		await deleteProduct(req.params.productId).then(result => res.send(result))
	}catch(err){
		res.status(500).json(err)
	}
})


module.exports = router;
