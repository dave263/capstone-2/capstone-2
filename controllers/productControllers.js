const CryptoJS = require('crypto-js');

const Product = require('../models/Product');

const Order = require('../models/Order');

const User = require('../models/User');



//CREATE A PRODUCT
module.exports.create = async (reqBody) => {
	const {productName, description, price} = reqBody

	let newProduct = new Product({
		productName: productName,
		description: description,
		price: price
	})
	return await newProduct.save().then((result, err) => result ? result : err)
}



//ALL ACTIVE PRODUCT
module.exports.getAllProducts = async () => {
	return await Product.find({isActive:true}).then(result => result)
}


//BOTH ACTIVE AND INACTIVE PRODUCTS
module.exports.getAll = async () => {
	return await Product.find().then(result => result)
}


//ALL ACTIVE PRODUCT IN HOME PAGE
module.exports.homeProducts = async () => {
	return await Product.find({isActive:true}).then(result => result)
}



//GET ONE PRODUCT
module.exports.oneProduct = async (id) => {
	return await Product.findById(id).then((result, err) => {
		if(result){
			return result
		} else {
			if(result == null){
				return {message: `Product is not in our records.`}
			}else{
				return err
			}
		}
	})
}





// USER CHECKOUT
module.exports.co = async (buy) => {
	const {userId, productId, productName, totalAmount} = buy

	const newOrder = new Order({
		userId: userId,
		productId: productId,
		productName: productName,
		totalAmount: totalAmount
	})
	return await newOrder.save().then((result, err) => result ? result : err)
}



// SEE ALL CHECKOUTS
module.exports.allco = async (uid) => {
	return await Order.find({userId: uid}, {productId: 1, productName: 1, totalAmount: 1}).then(result => result)
}


//CANCEL A CHECKOUT BY USER
module.exports.deleteCheckout = async (oid) => {
	return await Order.findByIdAndDelete(oid).then((result, err) => result ? true : err)
}



//GET ALL USER'S ORDERS BY ADMIN ONLY
module.exports.getAllOrders = async () => {
	return await Order.find().then(result => result)
}






//UPDATE A PRODUCT BY ADMIN ONLY
module.exports.updateProduct = async (productId, reqBody) => {
	return await Product.findByIdAndUpdate(productId, {$set: reqBody}, {new:true}).then(result => result)
}




//ARCHIVE PRODUCT BY ADMIN ONLY
module.exports.archiveProduct = async (productId) => {
	return await Product.findByIdAndUpdate(productId, {$set: {isActive: false}}, {new:true}).then(result => result)
}



//UNARCHIVE PRODUCT BY ADMIN ONLY
module.exports.unArchive = async (productId) => {
	return await Product.findByIdAndUpdate(productId, {$set: {isActive: true}}, {new:true}).then(result => result)
}



//DELETE PRODUCT BY ADMIN ONLY
module.exports.deleteProduct = async (productId) => {
	return await Product.findByIdAndDelete(productId).then((result, err) => result ? true : err)
}
