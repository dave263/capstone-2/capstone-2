const CryptoJS = require('crypto-js');

const User = require('../models/User');

const {createToken} = require('./../auth');


//GET ALL USERS
module.exports.getAllUsers = async () => {
	return await User.find().then(result => result)
}



//REGISTER A USER
module.exports.register = async (reqBody) => {
	const {firstName, lastName, email, password} = reqBody

	const newUser = new User({
		firstName: firstName,
		lastName: lastName, 
		email: email,
		password: CryptoJS.AES.encrypt(password, process.env.SECRET_PASS).toString()
	})
	return await  newUser.save().then(result => {
		if(result){
			return 'Thank you for registering!'
		} else {
			if(result == null){
				return 'Unable to register.'
			}
		}
	})
}



//CHECK IF EMAIL EXISTS
module.exports.checkEmail = async (reqBody) => {
	const {email} = reqBody
	return await User.findOne({email: email}).then((result, err) =>{
		if(result){
			return 'Email exists already.'
		} else {
			if(result == null){
				return 'Email does not exist yet.'
			} else {
				return err
			}
		}
	})
}



//LOGIN A USER
module.exports.login = async (reqBody) => {

	return await User.findOne({email: reqBody.email}).then((result, err) => {

		if(result == null){
			return {message: `User does not exist.`}

		} else {

			if(result !== null){
			
				const decryptedPw = CryptoJS.AES.decrypt(result.password, process.env.SECRET_PASS).toString(CryptoJS.enc.Utf8)

				console.log(reqBody.password == decryptedPw) //true
				

				if(reqBody.password == decryptedPw){
		
					return { token: createToken(result) }
				} else {
					return {auth: `Auth Failed!`}
				}

			} else {
				return err
			}
		}
	})
}



//RETRIEVE USER INFORMATION
module.exports.profile = async (id) => {

	return await User.findById(id).then((result, err) => {
		if(result){
			return result
		}else{
			if(result == null){
				return {message: `user does not exist`}
			} else {
				return err
			}
		}
	})
}



// UPDATE USER INFO
module.exports.updateUser = async (userId, reqBody) => {
	const userData = {
		firstName: reqBody.firstName,
		lastName: reqBody.lastName,
		email: reqBody.email,
		password: CryptoJS.AES.encrypt(reqBody.password, process.env.SECRET_PASS).toString()
	}

	return await User.findByIdAndUpdate(userId, {$set: userData}, {new:true}).then((result, err) => {
		// console.log(result)
		if(result){
			result.password = "***"
			return result
		} else{
			return err
		}
	})
}




// UPDATE PASSWORD
module.exports.updatePw = async (id, password) => {
	let updatedPw = {
		password: CryptoJS.AES.encrypt(password, process.env.SECRET_PASS).toString()
	}

	return await User.findByIdAndUpdate(id, {$set: updatedPw})
	.then((result, err) => {
		if(result){
			result.password = "***"
			return result
		} else{
			return err
		}
	})
}



//CHANGE TO ADMIN STATUS TO TRUE
module.exports.adminStatus = async (reqBody) => {
	const {email} = reqBody
	return await User.findOneAndUpdate({email: email}, {$set: {isAdmin: true}}, {new:true}).then((result, err) => result ? true : err)
}



//CHANGE TO ADMIN STATUS TO FALSE
module.exports.userStatus = async (reqBody) => {
	return await User.findOneAndUpdate({email: reqBody.email}, {$set: {isAdmin: false}}).then((result, err) => result ? true : err)
}




//DELETE A USER
module.exports.deleteUser = async (reqBody) => {
	const {email} = reqBody
	return await User.findOneAndDelete({email: email}).then((result, err) => result ? true : err)
}
