const express = require('express');
const mongoose = require('mongoose');
const dotenv = require('dotenv').config();
const cors = require('cors');

const PORT = process.env.PORT || 3009;
const app = express();


const userRoutes = require(`./routes/userRoutes`);
const productRoutes = require(`./routes/productRoutes`)


app.use(express.json());
app.use(express.urlencoded({extended:true}));
app.use(cors());


mongoose.connect(process.env.MONGO_URL, {useNewUrlParser: true, useUnifiedTopology: true});


const db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', () => console.log('Connected to Database'));


// Routes
    //create a middleware to be the root url of all routes
app.use(`/api/users`, userRoutes);
app.use(`/api/products`, productRoutes);



app.listen(PORT, () => console.log(`You are connected to port ${PORT}`))