const mongoose = require('mongoose');


const productSchema = new mongoose.Schema({
	productName:{
		type: String,
		required: [true, `Product name is required`],
		unique: true
	},
	description: {
		type: String,
		required: [true, `Product description is required`]
	},
	price: {
		type: Number,
		required: [true, `Price is required`]
	},
	isActive: {
		type: Boolean,
		default: true
	},
	createdOn: {
		type: Date,
		default: new Date()
	}
}, {timestamps: true})

module.exports = mongoose.model('Product', productSchema);