const mongoose = require('mongoose');


const orderSchema = new mongoose.Schema({
        userId: {
            type: String,
            required: [true, `userId is required`]
        },
        productId: {
            type: String,
            required: [true, `productId is required`]
        },
        productName: {
            type: String,
            required: [true, 'productName is required']
        },
        totalAmount: {
            type: Number,
            required: [true, `totalAmount is required`]
        }
}, {timestapms: true})

module.exports = mongoose.model('Order', orderSchema);